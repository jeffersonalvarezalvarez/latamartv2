class CreateAddress < ActiveRecord::Migration[5.0]
  def change
  	if Rails.env == "development"
		create_table :addresses do |t|
			t.string :address
			t.string :city
			t.string :country
			t.string :zip_code
			t.references :user, foreign_key: { to_table: :users }, index: true
			t.timestamps
		end
	end
  end
end
