class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
    	t.string :video_url
    	t.string :image_url
    	t.string :description
    end
  end
end
