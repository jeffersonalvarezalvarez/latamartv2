class AddFinishPaymentToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :finishPayment, :date
  end
end
