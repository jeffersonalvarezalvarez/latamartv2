class CheckAllToPublic < ActiveRecord::Migration[5.1]
  def change
  	Listing.all.each do |listing|
  		listing.public = true
  		listing.save
  	end
  end
end
