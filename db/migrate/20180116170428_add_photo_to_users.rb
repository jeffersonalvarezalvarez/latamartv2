class AddPhotoToUsers < ActiveRecord::Migration[5.0]
  def change
  	if Rails.env == "development"
    	add_column :users, :photo, :string
    end
  end
end
