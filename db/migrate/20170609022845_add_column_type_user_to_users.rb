class AddColumnTypeUserToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :type_user, :string
	add_column :users, :image_content_type, :string
	add_column :users, :image_file_size, :string
	add_column :users, :image_updated_at, :string
  end
end
