class AddColumnListingIdToPicture < ActiveRecord::Migration[5.0]
  def change
  	add_column :pictures, :listing_id, :integer
  end
end
