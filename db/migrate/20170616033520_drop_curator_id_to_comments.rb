class DropCuratorIdToComments < ActiveRecord::Migration[5.0]
  def change
  	remove_column :comments, :curator_id, :integer
  end
end
