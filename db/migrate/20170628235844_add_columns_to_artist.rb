class AddColumnsToArtist < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :telephone, :string
  	add_column :users, :description, :string
  	add_column :listings, :currency_id, :string
  end
end