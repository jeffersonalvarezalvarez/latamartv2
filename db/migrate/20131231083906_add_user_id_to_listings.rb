class AddUserIdToListings < ActiveRecord::Migration
  def change
    add_column :listings, :user_id, :integer
    add_column :listings, :image_content_type, :string
	add_column :listings, :image_file_size, :string
	add_column :listings, :image_updated_at, :string
  end
end
