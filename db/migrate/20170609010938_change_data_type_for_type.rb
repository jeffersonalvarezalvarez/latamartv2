class ChangeDataTypeForType < ActiveRecord::Migration[5.0]
  def self.up
    change_table :users do |t|
      t.change :type, :string
    end
  end
  def self.down
    change_table :users do |t|
      t.change :type, :integer
    end
  end
end