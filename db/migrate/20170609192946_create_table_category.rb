class CreateTableCategory < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.string :image_content_type
	  t.string :image_file_size
	  t.string :image_updated_at
      t.timestamps
    end
  end
end
