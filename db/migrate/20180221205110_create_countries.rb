class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
    	t.string :country
    end
    add_column :videos, :country_id, :integer
    add_column :videos, :title, :string
  end
end
