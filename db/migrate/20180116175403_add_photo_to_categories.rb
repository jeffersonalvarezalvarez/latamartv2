class AddPhotoToCategories < ActiveRecord::Migration[5.0]
  def change
  	if Rails.env == "development"
  		add_column :categories, :photo, :string
  	end
  end
end
