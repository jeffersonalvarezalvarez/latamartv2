# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180526000852) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", id: :serial, force: :cascade do |t|
    t.string "address"
    t.string "city"
    t.string "country"
    t.string "zip_code"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_addresses_on_user_id"
  end

  create_table "administrators", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "auth_token", limit: 32, null: false
    t.string "name", null: false
    t.string "username", null: false
    t.index ["auth_token"], name: "index_administrators_on_auth_token"
    t.index ["email"], name: "index_administrators_on_email", unique: true
    t.index ["reset_password_token"], name: "index_administrators_on_reset_password_token", unique: true
  end

  create_table "bank_accounts", id: :serial, force: :cascade do |t|
    t.string "bank"
    t.string "number"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "categories", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "image_content_type"
    t.string "image_file_size"
    t.string "image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo"
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.string "text"
    t.integer "qualification"
    t.integer "user_id"
    t.integer "listing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_name"
    t.integer "user_id_two"
    t.index ["listing_id"], name: "index_comments_on_listing_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "countries", id: :serial, force: :cascade do |t|
    t.string "country"
  end

  create_table "currencies", id: :serial, force: :cascade do |t|
    t.string "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "has_orders", id: :serial, force: :cascade do |t|
    t.integer "status"
    t.integer "listing_id"
    t.integer "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["listing_id"], name: "index_has_orders_on_listing_id"
    t.index ["order_id"], name: "index_has_orders_on_order_id"
  end

  create_table "listings", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.decimal "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "user_id"
    t.string "image_content_type"
    t.string "image_file_size"
    t.string "image_updated_at"
    t.integer "category_id"
    t.integer "subject_id"
    t.string "token"
    t.string "photo"
    t.integer "quantity"
    t.string "currency_id"
    t.boolean "public"
    t.index ["category_id"], name: "index_listings_on_category_id"
  end

  create_table "orders", id: :serial, force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "buyer_id"
    t.integer "seller_id"
    t.integer "address_id"
    t.integer "quantity"
  end

  create_table "pictures", id: :serial, force: :cascade do |t|
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "listing_id"
  end

  create_table "posts", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "recipient"
    t.string "first_name"
    t.string "last_name"
    t.string "type_user"
    t.string "image_content_type"
    t.string "image_file_size"
    t.string "image_updated_at"
    t.string "nick"
    t.string "provider"
    t.string "uid"
    t.string "telephone"
    t.string "description"
    t.string "socialn1"
    t.string "socialn2"
    t.string "socialn3"
    t.date "startPayment"
    t.date "finishPayment"
    t.boolean "statePayment"
    t.string "photo"
    t.boolean "public"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "videos", id: :serial, force: :cascade do |t|
    t.string "video_url"
    t.string "image_url"
    t.string "description"
    t.integer "country_id"
    t.string "title"
  end

  add_foreign_key "addresses", "users"
  add_foreign_key "comments", "listings"
  add_foreign_key "comments", "users"
  add_foreign_key "has_orders", "listings"
  add_foreign_key "has_orders", "orders"
end
