json.extract! comment, :id, :text, :qualification, :curator_id, :user_id, :listing_id, :created_at, :updated_at
json.url comment_url(comment, format: :json)
