class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.  
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

    def configure_permitted_parameters
      added_attrs_user_account_sign_up = [:first_name, :last_name, :photo, :type_user, :nick, :email, :socialn1, :socialn2, :socialn3, :terms_of_service]
      added_attrs_user_account_update = [:first_name, :last_name, :photo, :type_user, :nick, :email, :description, :telephone, :socialn1, :socialn2, :socialn3]
      devise_parameter_sanitizer.permit :sign_up, keys: added_attrs_user_account_sign_up
      devise_parameter_sanitizer.permit :account_update, keys: added_attrs_user_account_update
    end

    def after_sign_in_path_for(resource)
      if resource.class.name == "User"
          super(resource)
      elsif resource.class.name == "Administrator"
        cookies.encrypted[:admin_auth_token] = {
          value: current_administrator.auth_token,
          expires: params[:remember_me].eql?("1") ? 1.year.from_now : nil,
          httponly: true,
        }
        admin_users_path
      end
    end

end
