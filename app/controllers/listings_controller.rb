class ListingsController < ApplicationController
  before_action :set_listing, only: [:show, :edit, :update, :destroy, :edit_admin, :update_admin, :destroy_admin]
  before_action :authenticate_user!, only: [:seller, :new, :create, :edit, :update, :destroy]
  before_action :check_user, only: [:edit, :update, :destroy]
  before_action :check_payment, only: [:new]

  def seller
    @listings = Listing.where(user: current_user).order("created_at DESC")
  end

  def index
    @title = "LatamArt - Home"
    Listing.connection.execute "select setseed(0.#{(request.ip.to_s.split('.').last.to_i + Date.today.wday.to_i).to_s})"
    @listings = Listing.joins(:user).where(:listings => {:public => true},:users => {:public => true}).order("random()").page(params[:page]).per(12)
    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
      FROM latinamericanpost_db.fx2su_content
      INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
      WHERE catid in (194,301,223) and state = 1 order by id asc;")
    client.close
    respond_to do |format|
      format.html
      format.js
    end
  end

  def curated
    @title = "LatamArt - Staff picks"
    Listing.connection.execute "select setseed(0.#{(request.ip.to_s.split('.').last.to_i + Date.today.wday.to_i).to_s})"
    @listings = Listing.joins(:user).where(:users => {:statePayment => true, :public => true}, :listings => {:public => true}).order('random()').page(params[:page]).per(9)
    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
      FROM latinamericanpost_db.fx2su_content
      INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
      WHERE catid in (194,301,223) and state = 1 order by id asc;")
    client.close
    respond_to do |format|
      format.html
      format.js
    end
  end


  def subjects
    @selected = Subject.all.order("random()").page(params[:page]).per(6)

    respond_to do |format|
        format.js
    end
  end

  # Función para dividirlo en categorías
  def from_category

    if params[:cat_id] == "1"
      @selected = Listing.all.order("random()").page(params[:page]).per(8)
    else
      @selected = Listing.where(:category_id => params[:cat_id]).order(created_at: :desc).page(params[:page]).per(8)
    end 

    respond_to do |format|
        format.js
    end
  end

  # Función para dividirlo por precio
  def from_price
    minimium = Listing.all.minimum(:price)
    maximum = Listing.all.maximum(:price)
    range = ((maximum-minimium)/3).ceil

    if params[:range] == "range_price_1"
      @listings = Listing.where('price BETWEEN ? AND ?', minimium, minimium+range).page(params[:page]).per(12)
    elsif params[:range] == "range_price_2"
      @listings = Listing.where('price BETWEEN ? AND ?', minimium+range, minimium+(range*2)).page(params[:page]).per(12)
    elsif params[:range] == "range_price_3"
      @listings = Listing.where('price BETWEEN ? AND ?', minimium+(range*2), minimium+(range*3)).page(params[:page]).per(12)
    end 
  end 

  #Función para dividirlo por categorías
  def from_subject
    @listings = Listing.where('subject_id = ?', params[:subject]).order(:name).page(params[:page]).per(6)
  end

  #Función para la paginación de los comentarios
  def comments
    @selected = Comment.where("listing_id = ?", params[:listing_id]).order(created_at: :desc).page(params[:page]).per(9)

    respond_to do |format|
        format.js
    end

  end

  # GET /listings/1
  # GET /listings/1.json
  def show
    @comment = Comment.new
  end

  # GET /listings/new
  def new
    @listing = Listing.new
  end

  # GET /listings/1/edit
  def edit 
  end

  # POST /listings
  # POST /listings.json
  def create

    @listing = current_user.listings.new(listing_params)
    @listing.pictures << Picture.find(params[:photos].split(","))

    respond_to do |format|
      if @listing.save
        format.html { redirect_to @listing, notice: 'Listing was successfully created.' }
        format.json { render action: 'show', status: :created, location: @listing }
      else
        format.html { render action: 'new' }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update

    @listing.pictures << Picture.find(params[:photos].split(","))
    respond_to do |format|
      if @listing.update(listing_params)
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:name, :description, :price, :quantity, :category_id, :subject_id, :currency_id)
    end

    def check_user
      if current_user != @listing.user
        redirect_to root_url, alert: "Sorry, this listing belongs to someone else"
      end
    end

    # Use callbacks to prevent to add new artworks if payment is not present
    def check_payment
      if current_user.statePayment == false && current_user.listings.count >= 3
        redirect_to root_url, alert: "Sorry, you must contact to administrator to accomplish a payment, you have a maximum of 3 artworks"
      end
    end
end
