class UsersController < ApplicationController

  def show
  	@user = User.find_by_nick(params[:nick])
    if @user.present?
      @listings = @user.listings.where(:listings => {:public => true}).page(params[:page_listing]).per(6)
      @comments = @user.comments.page(params[:page_comment]).per(6)
      @comment = Comment.new
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

end