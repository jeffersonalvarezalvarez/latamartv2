class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def facebook
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.save(:validate => false)
      sign_in @user, :event => :authentication
      if @user.type_user.blank?
        redirect_to edit_user_registration_path
      else
        redirect_to root_path
      end
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def failure
    redirect_to root_path
  end
end