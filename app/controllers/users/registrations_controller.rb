class Users::RegistrationsController < Devise::RegistrationsController
  before_action :check_captcha, only: [:create] # Change this to be any actions you want to protect.

  def new
    super
  end

  def create
    super
  end

  private

    def check_captcha
      unless verify_recaptcha
        self.resource = resource_class.new sign_up_params
        respond_with_navigational(resource) { render :new }
      end 
    end

    def update_resource(resource, params)
      resource.update_without_password(params)
    end

end