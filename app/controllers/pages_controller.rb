class PagesController < ApplicationController

  def about
  end

  def exhibition
    language = params[:language]

    @title = "LatamArt - News"
    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    
    if language.present?
      @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
        FROM latinamericanpost_db.fx2su_content
        INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
        WHERE catid in (194,301,223) and latinamericanpost_db.fx2su_content.`language` = '#{language}' and state = 1 order by rand();")
    else
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
        FROM latinamericanpost_db.fx2su_content
        INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
        WHERE catid in (194,301,223) and state = 1 order by rand();")    
    end

    client.close
  end

  def appreciation
    @title = "LatamArt - Appreciation"
    @comments= Comment.all.order("random()").page(params[:page_comment]).per(6)

    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
      FROM latinamericanpost_db.fx2su_content
      INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
      WHERE catid in (194,301,223) and state = 1 order by id asc;")
    client.close
    respond_to do |format|
      format.html
      format.js
    end
  end
   
  def museums
    @title = "LatamArt - Museums"
    @countries = Country.all
    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
      FROM latinamericanpost_db.fx2su_content
      INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
      WHERE catid in (194,301,223) and state = 1 order by id asc;")
    client.close
  end

  def artist
    @title = "LatamArt - Artists"
    User.connection.execute "select setseed(0.#{(request.ip.to_s.split('.').last.to_i + Date.today.wday.to_i).to_s})"
    @artists = User.joins(:listings).where('CAST(type_user AS text) = \'1\' AND "users"."public" = true').where(:users => {:statePayment => false}).order('random()').group('"users"."id"').page(params[:page]).per(6)

    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
      FROM latinamericanpost_db.fx2su_content
      INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
      WHERE catid in (194,301,223) and state = 1 order by id asc;")
    client.close
    respond_to do |format|
      format.html
      format.js
    end
  end

  def top_artist
    @title = "LatamArt -Top artist"
    User.connection.execute "select setseed(0.#{(request.ip.to_s.split('.').last.to_i + Date.today.wday.to_i).to_s})"
    @artists = User.joins(:listings).where('CAST(type_user AS text) = \'1\' AND "users"."public" = true').where(:users => {:statePayment => true}).order('random()').group('"users"."id"').page(params[:page]).per(6)

    client = Mysql2::Client.new(:host => "latampostdbmysql.cw9xqocyq7hv.us-east-1.rds.amazonaws.com", :database => "latinamericanpost_db", :username => "latampost_user", :password => "?#22HrD,BqoG")
    @articles = client.query("SELECT latinamericanpost_db.fx2su_content.`id` AS `id`, latinamericanpost_db.fx2su_content.`title` AS `title`, `images`, `introtext`, `fulltext`, latinamericanpost_db.fx2su_categories.`alias` AS `category`, `created`, `created_by`, `state`, latinamericanpost_db.fx2su_content.`metadesc` AS `metadesc`, latinamericanpost_db.fx2su_content.`metakey` AS `metakey`, latinamericanpost_db.fx2su_content.`hits` AS `hits`, `publish_up`, latinamericanpost_db.fx2su_content.`language` AS `language`, latinamericanpost_db.fx2su_content.`alias` AS `alias` 
      FROM latinamericanpost_db.fx2su_content
      INNER JOIN latinamericanpost_db.fx2su_categories ON latinamericanpost_db.fx2su_content.catid = latinamericanpost_db.fx2su_categories.id
      WHERE catid in (194,301,223) and state = 1 order by id asc;")
    client.close
    respond_to do |format|
      format.html
      format.js
    end
  end
   

  def styles
    @categories = Category.all.order(:name)
    @listings = Listing.all.order("random()").page(params[:page]).per(6)
    @subjects = Subject.all.order("random()").page(params[:page]).per(6)
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def subjects
    @selected = Subject.all.order("random()").page(params[:page]).per(6)

    respond_to do |format|
        format.js
    end
  end


    def from_subject
    @listings = Listing.where('subject_id = ?', params[:subject]).order(:name).page(params[:page]).per(6)
  end


  def contact
  end

  def elearning
  end

  def pages_category    
    @subjects = Subject.joins("INNER JOIN listings ON subjects.id = listings.subject_id INNER JOIN categories ON categories.id = listings.category_id").where("categories.name LIKE ?", 
      params[:category].capitalize).group('id').page(params[:page]).per(6)
   
    @listings = Listing.joins(:category).where("categories.name LIKE ?", 
      params[:category].capitalize).page(params[:page]).per(6)
     respond_to do |format|
      format.html
      format.js

    end
  end
     
  def pages_subject    
    @listings = Listing.joins("INNER JOIN subjects ON subjects.id = listings.subject_id
          INNER JOIN categories ON categories.id = listings.category_id").where('subjects.name LIKE ?', params[:subject].capitalize).page(params[:page]).per(6)
    respond_to do |format|
      format.html
      format.js
    end
  end

 def search
    palabra = "%#{params[:keyword]}%"
    @results = Listing.where("name LIKE ?", palabra) + User.where("CAST(type_user AS text) = '1' AND (first_name LIKE ? OR last_name LIKE ? OR nick LIKE ?)", palabra, palabra, palabra) + Subject.where("name LIKE ?",palabra)
  end
end
