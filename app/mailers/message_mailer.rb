class MessageMailer < ApplicationMailer
	default from: "Noreply LatamArt<site@latamart.com>"
	
  	def new_message(message)
    	@message = message
   	 	mail subject: "Message from #{message.name}"
  	end

end
