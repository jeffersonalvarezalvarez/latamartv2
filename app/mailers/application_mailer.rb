class ApplicationMailer < ActionMailer::Base
  default from: 'LatamArt <site@latamart.com>'
  layout 'mailer'
end

