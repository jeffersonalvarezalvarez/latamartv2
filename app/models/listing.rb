class Listing < ActiveRecord::Base

  validates :name, :description, presence: true
  
  belongs_to :user
  belongs_to :category
  belongs_to :subject

  has_many :comments, :dependent => :destroy
  has_many :pictures, :dependent => :destroy

	manage_with_tolaria using: {
		icon: "image",
		category: "Content administration",
		priority: 1,
		permit_params: [
			:name,
			:description,
			:price,
			:public,
			:user_id,
			:category_id,
			:subject_id,
            :picture_ids => []
		]
	}

end
