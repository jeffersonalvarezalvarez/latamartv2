class Video < ActiveRecord::Base

	validates :video_url, :description, presence: true
	belongs_to :country
	
	manage_with_tolaria using: {
		icon: "image",
		category: "Content administration",
		priority: 1,
		permit_params: [
			:video_url,
			:description,
			:title,
			:country_id
		]
	}

end
