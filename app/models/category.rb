class Category < ApplicationRecord

  mount_uploader :photo, PictureCategoryUploader

  validates :name, presence: true
  validates :description, presence: true
  has_many :listings

	manage_with_tolaria using: {
		icon: "reorder",
		category: "Content administration",
		priority: 2,
		permit_params: [
			:name,
			:description,
			:photo
		]
	}

end
