class Picture < ApplicationRecord

	belongs_to :listing, optional: true
  	mount_uploader :image, PictureArtUploader

	def to_jq_upload
    {
	"url" => image.medium.url,
	"delete_url" => id,
	"picture_id" => id,
	"delete_type" => "DELETE"
    }.to_json
  	end

	manage_with_tolaria using: {
		icon: "file-image-o",
		category: "Content administration",
		priority: 4,
		permit_params: [
			:image
		]
	}

end