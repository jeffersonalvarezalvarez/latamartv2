class Subject < ApplicationRecord
	has_many :listings, dependent: :destroy
	mount_uploader :image, PictureArtUploader

	manage_with_tolaria using: {
		icon: "newspaper-o",
		category: "Content administration",
		priority: 3,
		permit_params: [
			:name,
			:image
		]
	}
end
