class Country < ActiveRecord::Base

  validates :country, presence: true
  has_many :videos, :dependent => :destroy

	manage_with_tolaria using: {
		icon: "image",
		category: "Content administration",
		priority: 1,
		permit_params: [
			:country
		]
	}

end