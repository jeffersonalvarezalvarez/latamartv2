class BankAccount < ActiveRecord::Base

  validates :bank, presence: true
  validates :number, presence: true
  validates :country, presence: true

  belongs_to :user

	manage_with_tolaria using: {
	icon: "money",
	category: "User administration",
	priority: 3,
		permit_params: [
			:bank,
			:number,
			:country,
			:user_id
		]
	}
	
end