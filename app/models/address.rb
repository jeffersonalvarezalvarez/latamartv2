class Address < ApplicationRecord
	belongs_to :user

	manage_with_tolaria using: {
	icon: "globe",
	category: "User administration",
	priority: 2,
		permit_params: [
			:address,
			:city,
			:country, 
			:zip_code,
			:user_id
		]
	}

end
