class User < ActiveRecord::Base

  attr_accessor :login
  attr_accessor :terms

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :omniauthable, :omniauth_providers => [:facebook],
         :authentication_keys => [:login], :reset_password_keys => [:email]

  mount_uploader :photo, PictureUploader
  
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :type_user, presence: true

  validates_uniqueness_of :nick, :allow_blank => true
  validates_format_of :nick, with: /^[a-zA-Z0-9_\.]*$/, :multiline => true
  validate :nick

  validates :terms_of_service, acceptance: true

  has_many :listings, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :addresses, :dependent => :destroy
  has_many :bank_accounts, :dependent => :destroy
  
  manage_with_tolaria using: {
    icon: "users",
    category: "User administration",
    priority: 1,
    permit_params: [
        :photo,
        :first_name,
        :last_name,
        :email,
        :nick,
        :socialn1,
        :socialn2,
        :socialn3,
        :startPayment,
        :finishPayment,
        :statePayment,
        :description,
        :telephone,
        :type_user,
        :password,
        :public
    ]
  }

  def self.find_for_database_authentication warden_conditions
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    where(conditions).where(["lower(nick) = :value OR lower(email) = :value", {value: login.strip.downcase}]).first
  end

  def validate_username
    if User.where(email: nick).exists?
      errors.add(:nick, :invalid)
    end
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.first_name = auth.info.first_name 
      user.last_name = auth.info.last_name 
      user.remote_photo_url = process_uri(auth.info.image) # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails, 
      # uncomment the line below to skip the confirmation emails.
      user.skip_confirmation!
    end
  end

  private

    def self.process_uri(uri)
      avatar_url = URI.parse(uri)
      avatar_url.scheme = 'https'
      avatar_url.to_s
    end

end