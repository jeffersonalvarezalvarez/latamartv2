class Currency < ActiveRecord::Base

  validates :currency, presence: true
  has_many :listings, :dependent => :destroy

end